﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace generator_danych
{
    class Program
    {
        static void Main(string[] args)
        {
            Random gRandom = new Random();

            try
            {
                Matches matches = new Matches();
                Judges judges = new Judges();
                Teams teams = new Teams();
                Console.WriteLine("Wczytano {0} sędziów i {1} drużyn", judges.Count, teams.Count);


                Console.WriteLine("Wpisz rok, w którym chcesz wygenerować mecze: ");    
                int year = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Ile meczy generujemy: ");
                int count = Int32.Parse(Console.ReadLine());

                Console.WriteLine("No to generuję {0} meczy w roku {1}", count, year);

                for(int i = 0; i < count; i++)
                {
                    gRandom.Next();
                    while(!matches.TryAdd(
                        new Match(
                            GetRandomDate(year, gRandom), 
                            teams.GetRandom(gRandom), 
                            teams.GetRandom(gRandom), 
                            judges.GetRandom(gRandom),
                            gRandom))
                        )
                    {
                        Console.WriteLine("Błędne dane, próbuję raz jeszcze...");
                    }
                }

                matches.WriteMatches();

                matches.Save();

                Console.WriteLine("Zapisano do bazy danych...");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadKey();

        }


        static DateTime GetRandomDate(int year, Random rand)
        {
            int day = rand.Next(1, 28);
            int month = rand.Next(1, 12);
            return new DateTime(year, month, day);
        }



    }
}
