﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace generator_danych
{
    class Judge
    {
        int id;
        string firstName, lastName;

        public int Id { get => id; }
        public string FirstName { get => firstName; }
        public string LastName { get => lastName; }

        public Judge(int id, string fname, string lname)
        {
            this.id = id;
            this.firstName = fname;
            this.lastName = lname;
        }
    }
}
