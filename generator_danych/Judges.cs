﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace generator_danych
{
    class Judges
    {
        List<Judge> judges;
        public int Count { get => this.judges.Count; }

        public Judges()
        {
            this.judges = new List<Judge>();
           

            using (OracleConnection connection = new OracleConnection(Const.OracleConnectionString))
            {
                connection.Open();
                string SQL = "select * from JUDGES";

                using (OracleCommand command = new OracleCommand(SQL, connection))
                {
                    OracleDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        int id = Int32.Parse(rdr["ID"].ToString());

                        this.judges.Add(new Judge(
                            Int32.Parse(rdr["ID"].ToString()),
                            rdr["FIRST_NAME"].ToString(),
                            rdr["LAST_NAME"].ToString()));
                    }
                }
            }
        }

        public Judge GetRandom(Random rand)
        {
            int index = rand.Next(this.judges.Count);
            return this.judges[index];
        }

    }
}
