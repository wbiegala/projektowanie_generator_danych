﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace generator_danych
{
    class Team
    {
        int id;
        string name;

        List<Player> players;

        public int Id { get => this.id; }

        public Team(int id, string name)
        {
            this.id = id;
            this.name = name;
            players = new List<Player>();
            this.GetPlayers();
        }

        public void GetPlayers()
        {
            using (OracleConnection connection = new OracleConnection(Const.OracleConnectionString))
            {
                connection.Open();
                string SQL = "select * from PLAYERS where TEAM_ID=" + this.id;

                using (OracleCommand command = new OracleCommand(SQL, connection))
                {
                    OracleDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        int id = Int32.Parse(rdr["ID"].ToString());

                        this.players.Add(new Player(
                            Int32.Parse(rdr["ID"].ToString()),
                            rdr["FIRST_NAME"].ToString(),
                            rdr["LAST_NAME"].ToString()));
                    }
                }
            }
        }

        public Player GetRandom(Random rand)
        {
            int index = rand.Next(this.players.Count);
            return this.players[index];
        }

    }
}
