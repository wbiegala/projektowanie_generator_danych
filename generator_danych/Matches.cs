﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generator_danych
{
    class Matches
    {
        List<Match> matches;

        public Matches()
        {
            this.matches = new List<Match>();
        }

        public bool TryAdd(Match toAdd)
        {
            List<Match> sameDayMatches = new List<Match>(matches.FindAll(x => x.Date == toAdd.Date));

            if (toAdd.Host == toAdd.Guest)
            {
                return false;
            }

            if (sameDayMatches.Exists(x =>
            (x.Host == toAdd.Host ||
            x.Host == toAdd.Guest ||
            x.Guest == toAdd.Host ||
            x.Guest == toAdd.Guest ||
            x.Judge == toAdd.Judge)))
            {
                return false;
            }

            this.matches.Add(toAdd);
            return true;            
        }

        public void Save()
        {
            foreach (var match in this.matches)
            {
                match.Save();
            }
        }


        public void WriteMatches()
        {
            foreach (var match in this.matches)
            {
                Console.WriteLine(match.Write());
            }
        }

    }
}
